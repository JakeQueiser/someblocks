//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
    { "", "mpstat | awk '/all/ { print 100.0 - $13 }'", 5, 0},
    { "", "sensors | awk '/^CPU/ { cpu = $2 } /^GPU/ { gpu = $2 } /^fan1/ { fan1 = $2 } /^fan2/ { fan2 = $2 } END { print cpu, gpu, fan1, fan2 }'", 5, 0},
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"", "free -h | awk '/^Mem/ { print $3\"/\"$2 }' | sed s/i//g",	5,		0},
    { "", "acpi -b | awk '/^Battery 0/ { print $3\": \"$4\" \"$5}' | sed s/,//g", 5, 0},
	{"", "date '+%b %d (%a) %I:%M%p'",					5,		0},
	
	/* Updates whenever "pkill -SIGRTMIN+10 someblocks" is ran */
	/* {"", "date '+%b %d (%a) %I:%M%p'",					0,		10}, */
};



//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
